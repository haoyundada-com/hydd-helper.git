<?php

namespace haoyundada\contract;

interface Arrayable
{
    public function toArray(): array;
}
